import java.util.ArrayList;

/**
 * Abstract class of data file parser
 */
public abstract class Parser {

    /**
     * Max vertex count to analyze
     */
    protected static final int maxAnalizeVertex = 250000;

    /**
     * Max face count to analyze
     */
    protected static final int maxFacesCount = 1500;

    /**
     * Flag of voxels loading
     * <p>
     * True if loaded, else - false
     */
    protected boolean voxelsLoaded;

    /**
     * Return flag of voxels loading
     * @return true if loaded, else - false
     */
    public boolean isVoxelsLoaded() {
        return this.voxelsLoaded;
    }

    /**
     * Loaded voxels array
     */
    protected ArrayList<Voxel> loadedVoxelsArray;

    /**
     * Return loaded voxels array if it was loaded, else - null
     * @return voxel array or null
     */
    public ArrayList<Voxel> getLoadedVoxelArray() {
        if(this.voxelsLoaded) {
            return this.loadedVoxelsArray;
        }
        return null;
    }

    /**
     * Flag of model loading
     * <p>
     * True if loaded, else - false
     */
    protected boolean modelLoaded;

    /**
     * Return flag of model loading
     * @return true if loaded, else - false
     */
    public boolean isModelLoaded() {
        return this.modelLoaded;
    }

    /**
     * Loaded model
     */
    protected Model loadedModel;

    //getting loaded model (if model wasn't loaded will return null)

    /**
     * Return loaded model if it was loaded, else - null
     * @return loaded model or null
     */
    public Model getLoadedModel() {
        if(this.modelLoaded) {
            return this.loadedModel;
        }
        return null;
    }

    //flag of state of loading vertex cloud
    /**
     * Flag of vertex cloud loading
     * <p>
     * True if loaded, else - false
     */
    protected boolean vertexCloudLoaded;

    /**
     * Return flag of vertex cloud loading
     * @return true if loaded, else - false
     */
    public boolean isVertexCloudLoaded() {
        return this.vertexCloudLoaded;
    }

    /**
     * Loaded vertex cloud
     */
    protected ArrayList<Vertex> loadedVertexCloud;

    /**
     * Return loaded vertex cloud if it was loaded, else - null
     * @return loaded vertex cloud or null
     */
    public ArrayList<Vertex> getLoadedVertexCloud() {
        if(this.vertexCloudLoaded) {
            return this.loadedVertexCloud;
        }
        return null;
    }

    /**
     * Constructor oif class
     */
    public Parser() {
        this.loadedVertexCloud = new ArrayList<Vertex>();
        this.loadedModel = new Model();
        this.loadedVoxelsArray = new ArrayList<Voxel>();
        this.modelLoaded = false;
        this.vertexCloudLoaded = false;
        this.voxelsLoaded = false;
    }

    /**
     * Load data from file
     * @param fileName path to file
     * @return true if success, else - false
     */
    public abstract boolean load(String fileName);

    /**
     * Fail of parsing file
     * @return false
     */
    protected boolean fail() {
        this.vertexCloudLoaded = false;
        this.modelLoaded = false;
        this.voxelsLoaded = false;
        this.loadedVertexCloud.clear();
        this.loadedModel = null;
        this.loadedVoxelsArray = null;
        return false;
    }

}
