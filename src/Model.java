import java.util.ArrayList;
import java.util.HashMap;

/**
 * Polygonal model of 3D-object
 */
public class Model {

    /**
     * Set of model's faces (polygons)
     */
    private ArrayList<Face> faces;

    /**
     * Return set of model's faces (polygons)
     * @return array of faces
     */
    public ArrayList<Face> getFaces() {
        return faces;
    }

    /**
     * Count of model's faces
     */
    private int facesCount;

    /**
     * Return count of model's faces
     * @return count of model's faces
     */
    public int getFacesCount() {
        return facesCount;
    }

    /**
     * Constructor of class
     */
    public Model() {
        this.faces = new ArrayList<>();
        this.facesCount = 0;
    }

    /**
     * Add new face to model
     * @param newFace new face
     */
    public void addFace(Face newFace){
        this.faces.add(newFace);
        this.facesCount ++;
    }

    /**
     * Remove face with index from face
     * @param index index of face
     * @return true - success, false - not exists face with that index
     */
    public boolean removeFace(int index) {
        if(index >= this.facesCount) {
            return false;
        }
        this.faces.remove(index);
        this.facesCount --;
        return true;
    }

}



