import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class of container of 3D object
 * <p>
 * After success loading of 3D-object container always contains set of vertexes (vertex cloud)
 * and can contain 3D-model object if loaded from file model consists of small count of polygons
 * and voxel array after constructing by polygons of model or loading from file with model with large counbt of polygons
 */
public class Container3D {

    /**
     * precision on analyze
     */
    private double precision = CompareConfig.getPrecision();

    /**
     * voxels counts by axis
     */
    private int countX; /* by axis Ox */
    private int countY; /* by axis Oy */
    private int countZ; /* by axis Oz */

    /**
     * Valuesof borders of 3D-object
     */
    private HashMap<String, Double> borderValues;

    /**
     * flag of loading voxel array
     * <p>
     * true - loaded, else - false
     */
    protected boolean voxelsLoaded;

    /**
     * Return flag of loading voxel array
     * @return true - loaded, else - false
     */
    public boolean isVoxelsLoaded() {
        return this.voxelsLoaded;
    }

    /**
     * Loaded/constructed array of voxels
     */
    protected ArrayList<Voxel> loadedVoxelsArray;

    //getting loaded vertex cloud (if it wasn't loaded will return null)

    /**
     * Return loaded/constructed vertex cloud or if it wasn't loaded - null
     * @return loaded/constructed voxel array or null
     */
    public ArrayList<Voxel> getLoadedVoxelArray() {
        if(this.voxelsLoaded) {
            return this.loadedVoxelsArray;
        }
        return null;
    }

    /**
     * Set voxel array
     * @param voxelArray new voxel array
     */
    public void uploadVoxelsArray(ArrayList<Voxel> voxelArray) {
        this.loadedVoxelsArray = voxelArray;
        this.voxelsLoaded = true;
    }

    /**
     * Flag of loading of 3D-model
     * <p>
     * true - loaded, else - false
     */
    private boolean modelLoaded;

    /**
     * Return flag of loading of 3D-model
     * @return true - loaded, else - false
     */
    public boolean isModelLoaded() {
        return this.modelLoaded;
    }

    /**
     * Loaded 3D-model
     */
    private Model loadedModel;

    /**
     * Return loaded 3D-model or null if it is not loaded
     * @return Lodaed model or null
     */
    public Model getLoadedModel() {
        if(this.modelLoaded) {
            return this.loadedModel;
        }
        return null;
    }

    /**
     * Set 3D-model
     * @param newModel new 3D-model
     */
    public void uploadModel(Model newModel) {
        this.loadedModel = newModel;
        this.modelLoaded = true;
    }

    /**
     * Center of 3D object (vertex cloud)
     */
    private Vertex centerCloud;

    /**
     * Flag of state of loading vertex cloud
     * <p>
     * true if uploaded else - false
     */
    private boolean vertexCloudLoaded;

    /**
     * Return flag of state of loading vertex cloud
     * @return true if uploaded else - false
     */
    public boolean isVertexCloudLoaded() {
        return this.vertexCloudLoaded;
    }

    /**
     * Loaded vertex cloud
     */
    private ArrayList<Vertex> loadedVertexCloud;

    //getting loaded vertex cloud (if it wasn't loaded will return null)

    /**
     * Return vertex cloud or null if in is not loaded
     * @return Vertex cloud or null
     */
    public ArrayList<Vertex> getLoadedVertexCloud() {
        if(this.vertexCloudLoaded) {
            return this.loadedVertexCloud;
        }
        return null;
    }

    /**
     * Set vertex cloud
     * @param newCloud new vertex cloud
     */
    public void uploadVertexCloud(ArrayList<Vertex> newCloud) {
        this.loadedVertexCloud = newCloud;
        this.vertexCloudLoaded = true;
    }

    /**
     * Constructior of class
     */
    public Container3D() {
        this.loadedVertexCloud = new ArrayList<Vertex>();
        this.loadedModel = new Model();
        this.modelLoaded = false;
        this.vertexCloudLoaded = false;
    }

    /**
     * Return borders of 3D-object
     * @return HashMap with border values of 3D-object
     */
    public HashMap<String, Double> getBorderValues() {
        return this.borderValues;
    }

    /**
     * Calculate border valuse of vertex cloud
     */
    public void constructBorderValues() {
        if(this.isVertexCloudLoaded()) {
            this.borderValues = new HashMap<String, Double>();
            //X
            this.borderValues.put("min_x", this.loadedVertexCloud.get(0).getX());
            this.borderValues.put("max_x", this.loadedVertexCloud.get(0).getX());
            //Y
            this.borderValues.put("min_y", this.loadedVertexCloud.get(0).getY());
            this.borderValues.put("max_y", this.loadedVertexCloud.get(0).getY());
            //Z
            this.borderValues.put("min_z", this.loadedVertexCloud.get(0).getZ());
            this.borderValues.put("max_z", this.loadedVertexCloud.get(0).getZ());
            for(Vertex vertex : this.loadedVertexCloud) {
                //X
                if(vertex.getX() < this.borderValues.get("min_x")) {
                    this.borderValues.replace("min_x", vertex.getX());
                }
                if(vertex.getX() > this.borderValues.get("max_x")) {
                    this.borderValues.replace("max_x", vertex.getX());
                }
                //Y
                if(vertex.getY() < this.borderValues.get("min_y")) {
                    this.borderValues.replace("min_y", vertex.getY());
                }
                if(vertex.getY() > this.borderValues.get("max_y")) {
                    this.borderValues.replace("max_y", vertex.getY());
                }
                //Z
                if(vertex.getZ() < this.borderValues.get("min_z")) {
                    this.borderValues.replace("min_z", vertex.getZ());
                }
                if(vertex.getZ() > this.borderValues.get("max_z")) {
                    this.borderValues.replace("max_z", vertex.getZ());
                }
            }
            this.centerCloud = new Vertex();
            this.centerCloud.setX((this.borderValues.get("max_x") - this.borderValues.get("min_x")) / 2);
            this.centerCloud.setY((this.borderValues.get("max_y") - this.borderValues.get("min_y")) / 2);
            this.centerCloud.setZ((this.borderValues.get("max_z") - this.borderValues.get("min_z")) / 2);
        }
    }

    /**
     * Construct voxel array
     * @return true - success, else - false
     */
    public boolean constructVoxels() {
        if (!this.findSegmentCoordsCounts(this.getBorderValues())) {
            return false;
        }
        /* init voxel array */
        this.loadedVoxelsArray = new ArrayList<Voxel>();
        for (int i = 0; i <= this.countX + 1; i ++) {//x axle
            for(int j = 0; j <= this.countY + 1; j ++) {//y axle
                for (int k = 0; k <= this.countZ + 1; k ++) {//z axle
                    /* constructing voxel */
                    Vertex newVoxelCenter = new Vertex();
                    newVoxelCenter.setX(this.borderValues.get("min_x") + (this.precision * i));
                    newVoxelCenter.setY(this.borderValues.get("min_y") + (this.precision * j));
                    newVoxelCenter.setZ(this.borderValues.get("min_z") + (this.precision * k));
                    Voxel voxel = new Voxel(newVoxelCenter, this.precision);
                    /* checking voxel */
                    for(Face face : this.loadedModel.getFaces()) {
                        if(!face.isActive()){
                            continue;
                        }
                        Vertex projection = face.getVertexProjection(voxel.getCenter());
                        if(projection == null){
                            continue;
                        }
                        /* if projection within voxel */
                        if(voxel.checkVertex(projection)) {
                            /* voxel is within one of model faces */
                            voxel.resetVertexCounter();
                            this.loadedVoxelsArray.add(voxel);
                            face.addVoxel(this.loadedVoxelsArray.size() - 1);
                            break;
                        }
                        /* voxel is within on this face */
                    }
                    //voxel is not within in any faces of model
                }
            }
        }
        this.voxelsLoaded = true;
        return true;
    }

    /**
     * Scaling
     * @param coefX scaling coefficient by X
     * @param coefY scaling coefficient by Y
     * @param coefZ scaling coefficient by Z
     */
    public void scale(double coefX, double coefY, double coefZ) {
        for (Vertex vertex : this.loadedVertexCloud) {
            vertex.scale(coefX, coefY, coefZ);
        }
        if(this.isModelLoaded()) {
            for (Face face : this.loadedModel.getFaces()) {
                face.findMathParams();
            }
        }
        this.constructBorderValues();
    }

    /**
     * Return coordinates of center of 3D-object
     * @return Coordinates of center of 3D-object
     */
    public Vertex getCenterCoords() {
        return this.centerCloud;
    }

    /**
     * Trim noise vertex from cloud
     * <p>
     * Statistic trimming of vertexes remoted from expected center of 3D-object
     * longer than multiplication middle deviation on thin coefficient from CompareConfig
     * @return radius of trimed region of 3D-object
     * @see CompareConfig
     */
    public double thinVertexCloud()
    {
        /* expected values by coords */
        double MO_X = 0;
        double MO_Y = 0;
        double MO_Z = 0;
        /* standart deviation by coords */
        double SKO_X = 0;
        double SKO_Y = 0;
        double SKO_Z = 0;
        if(this.isModelLoaded()) {
            return Double.NaN;
        }
        /* expected values */
        for (Vertex vertex: this.loadedVertexCloud) {
            MO_X += vertex.getX();
            MO_Y += vertex.getY();
            MO_Z += vertex.getZ();
        }
        MO_X /= this.loadedVertexCloud.size();
        MO_Y /= this.loadedVertexCloud.size();
        MO_Z /= this.loadedVertexCloud.size();
        /* standart deviations */
        for (Vertex vertex: this.loadedVertexCloud) {
            SKO_X += Math.pow(vertex.getX() - MO_X , 2);
            SKO_Y += Math.pow(vertex.getY() - MO_Y , 2);
            SKO_Z += Math.pow(vertex.getZ() - MO_Z , 2);
        }
        SKO_X = Math.sqrt(SKO_X / this.loadedVertexCloud.size());
        SKO_Y = Math.sqrt(SKO_Y / this.loadedVertexCloud.size());
        SKO_Z = Math.sqrt(SKO_Z / this.loadedVertexCloud.size());
        /* expected center vertex */
        Vertex MO_center = new Vertex();
        MO_center.setX(MO_X);
        MO_center.setY(MO_Y);
        MO_center.setZ(MO_Z);
        /* standatt deviation radius */
        double SKO_dist = Math.sqrt(Math.pow(SKO_X, 2) + Math.pow(SKO_Y, 2) + Math.pow(SKO_Z, 2));
        ArrayList<Vertex> newVertexArray = new ArrayList<Vertex>();
        for (Vertex vertex: this.loadedVertexCloud) {
            if(vertex.getDistanceTo(MO_center) < CompareConfig.getThinCoef() * SKO_dist) {
                /* new center */
                vertex.setX(vertex.getX() - MO_X);
                vertex.setY(vertex.getY() - MO_Y);
                vertex.setZ(vertex.getZ() - MO_Z);
                newVertexArray.add(vertex);
            }
        }
        this.loadedVertexCloud = newVertexArray;
        this.constructBorderValues();
        this.centerCloud = MO_center;
        return SKO_dist;
    }

    /**
     * Finding counts of voxels by axis
     * @param borders 3D-object border values
     * @return true - success, else - false
     */
    public boolean findSegmentCoordsCounts(HashMap<String, Double> borders) {
        /* keys test */
        if(!borders.containsKey("min_x") || !borders.containsKey("max_x") ||
                !borders.containsKey("min_y") || !borders.containsKey("max_y") ||
                !borders.containsKey("min_z") || !borders.containsKey("max_z")) {
            return false;
        }
        /* voxels on axis counts */
        this.countX = (int)((borders.get("max_x") - borders.get("min_x")) / precision);
        this.countY = (int)((borders.get("max_y") - borders.get("min_y")) / precision);
        this.countZ = (int)((borders.get("max_z") - borders.get("min_z")) / precision);
        return true;
    }

}
