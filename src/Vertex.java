/**
 * Class  of 3D-point
 */
public class Vertex {

    /**
     * Vertex status
     * <p>
     * Not active face will not take part in next actions
     * <p>
     * True - active, else - false
     */
    private boolean active = true;

    /**
     * Vertex coordinates
     */
    private double x; /* X coordinate */
    private double y; /* Y coordinate */
    private double z; /* Z coordinate */

    /**
     * Vertex colors
     */
    private int red = 0; /* red color */
    private int green = 0; /* green color */
    private int blue = 0; /* blue color */

    /**
     * Return X coord
     * @return X coord
     */
    public double getX() {
        return x;
    }

    /**
     * Set new X coord
     * @param x new X coord
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Return Y coord
     * @return Y coord
     */
    public double getY() {
        return y;
    }

    /**
     * Set new Y coord
     * @param y new Y coord
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Return Z coord
     * @return Z coord
     */
    public double getZ() {
        return z;
    }

    /**
     * Set new Z coord
     * @param z new Z coord
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * @return red color
     */
    public int getRed() {
        return red;
    }

    /**
     * Set red color
     * @param red
     */
    public void setRed(int red) {
        if(red < 0) {
            this.red = 0;
            return;
        }
        if(red > 255) {
            this.red = 255;
            return;
        }
        this.red = red;
    }

    /**
     * @return green color
     */
    public int getGreen() {
        return green;
    }

    /**
     * Set green color
     * @param green
     */
    public void setGreen(int green) {
        if(green < 0) {
            this.green = 0;
            return;
        }
        if(green > 255) {
            this.green = 255;
            return;
        }
        this.green = green;
    }

    /**
     * @return blue color
     */
    public int getBlue() {
        return blue;
    }

    /**
     * Set blue color
     * @param blue
     */
    public void setBlue(int blue) {
        if(blue < 0) {
            this.blue = 0;
            return;
        }
        if(blue > 255) {
            this.blue = 255;
            return;
        }
        this.blue = blue;
    }

    /**
     * Face status
     * <p>
     * Not active face will not take part in next actions
     * @return true - active, else - false
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * Deactivate vertex
     */
    public void deactive() {
        this.active = false;
    }

    /**
     * Scale coords
     * @param coefX
     * @param coefY
     * @param coefZ
     */
    public void scale(double coefX, double coefY, double coefZ) {
        this.x = this.x * coefX;
        this.y = this.y * coefY;
        this.z = this.z * coefZ;
    }

    /**
     * Find and return distanse form this to vertex
     * @param vertex
     * @return distacse
     */
    public double getDistanceTo(Vertex vertex) {
        return Math.sqrt(Math.pow(vertex.getX() - this.x, 2) + Math.pow(vertex.getY() - this.y, 2) + Math.pow(vertex.getZ() - this.z, 2));
    }

    /**
     * Rotate vertex
     * @param angle
     * @param xCenter rotation center X coord
     * @param yCenter rotation center Y coord
     */
    public void rotateZ(double angle, double xCenter, double yCenter) {
        double x = this.getX();
        double y = this.getY();
        double newX = xCenter + (((x - xCenter) * Math.cos(angle)) - ((y - yCenter) * Math.sin(angle)));
        double newY = yCenter + (((x - xCenter) * Math.sin(angle)) + ((y - yCenter) * Math.cos(angle)));
        this.setX(newX);
        this.setY(newY);
    }

}