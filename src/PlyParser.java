import java.io.BufferedReader;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Parser of ply files extends abstract class parser
 */
public class PlyParser extends Parser {

    /**
     * Load vertex cloud and model from file
     * @param filename path to file
     * @return true if success, else - false
     */
    public boolean load(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String fileString = reader.readLine();
            while (fileString != null) {
                String[] stringParts = fileString.split(" ");
                if(stringParts[0].equals("format")){
                    /* inner encoding */
                    switch (stringParts[1]){
                        case "binary_little_endian" : {
                            reader.close();
                            return this.binaryParsing(filename, ByteOrder.LITTLE_ENDIAN);
                        }
                        case "binary_big_endian" : {
                            reader.close();
                            return this.binaryParsing(filename, ByteOrder.BIG_ENDIAN);
                        }
                        case "ascii" :{
                            reader.close();
                            return this.asciiParsing(filename);
                        }
                        default:{
                            reader.close();
                            return false;
                        }
                    }
                }
                fileString = reader.readLine();
            }
        }
        catch (Exception e){
            return this.fail();
        }
        return false;
    }

    /**
     * Return result of ascii loading
     * @param filename path to file
     * @return true if success, else - false
     */
    private boolean asciiParsing(String filename) {
        /* vertex clous thin coefs */
        int analizeThinCoef = 1;
        /* parcing mode */
        boolean voxelMode = false;
        /* elements */
        HashMap<String, Integer> elementsCounts = new HashMap<String, Integer>();
        ArrayList<String> elementNames = new ArrayList<String>();
        /* properties */
        HashMap<String, HashMap<String, Integer>> properties = new HashMap<String, HashMap<String, Integer>>();
        int propertyCounter = 0;
        /* flags */
        boolean headerFlag = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String fileString = reader.readLine();
            while (fileString != null) {
                String[] stringParts = fileString.split(" ");
                /* anaize */
                //header
                if(headerFlag) {
                    //element
                    if(stringParts[0].equals("element")){
                        elementsCounts.put(stringParts[1], Integer.parseInt(stringParts[2]));
                        properties.put(stringParts[1], new HashMap<String, Integer>());
                        elementNames.add(stringParts[1]);
                        propertyCounter = 0;
                    }
                    //property
                    if(stringParts[0].equals("property")){
                        if(stringParts.length <= 3){
                            properties.get(elementNames.get(elementNames.size() - 1)).put(stringParts[2], propertyCounter);
                            propertyCounter++;
                        }
                    }
                    if(stringParts[0].equals("end_header")) {
                        if(!elementsCounts.containsKey("face")){
                            analizeThinCoef = (int)(elementsCounts.get("vertex") / maxAnalizeVertex);
                        } else {
                            if(elementsCounts.get("face") > maxFacesCount) {
                                voxelMode = true;
                            }
                        }
                        headerFlag = false;
                    }
                } else {
                    //data
                    for (String elementName : elementNames ) {
                        if(elementsCounts.get(elementName) > 0) {
                            switch (elementName){
                                case "vertex": {//vertex
                                    Vertex vertex = new Vertex();
                                    //coordinates
                                    vertex.setX(Double.parseDouble(stringParts[properties.get(elementName).get("x")]));
                                    vertex.setY(Double.parseDouble(stringParts[properties.get(elementName).get("y")]));
                                    vertex.setZ(Double.parseDouble(stringParts[properties.get(elementName).get("z")]));
                                    if(properties.get(elementName).containsKey("red")) {
                                        vertex.setRed(Integer.parseInt(stringParts[properties.get(elementName).get("red")]));
                                    }
                                    if(properties.get(elementName).containsKey("green")) {
                                        vertex.setRed(Integer.parseInt(stringParts[properties.get(elementName).get("green")]));
                                    }
                                    if(properties.get(elementName).containsKey("blue")) {
                                        vertex.setRed(Integer.parseInt(stringParts[properties.get(elementName).get("blue")]));
                                    }
                                    if(elementsCounts.get(elementName) % analizeThinCoef == 0) {
                                        this.loadedVertexCloud.add(vertex);
                                    }
                                    this.vertexCloudLoaded = true;
                                }; break;
                                case "face" : {
                                    if(!voxelMode) {
                                        Face face = new Face();
                                        for (int i = 1; i <= Integer.parseInt(stringParts[0]); i++) {
                                            face.addVertex(this.loadedVertexCloud.get(Integer.parseInt(stringParts[i])));
                                        }
                                        face.findMathParams();
                                        this.loadedModel.addFace(face);
                                        this.modelLoaded = true;
                                    }
                                }; break;
                            }
                            int currentCount = elementsCounts.get(elementName);
                            elementsCounts.replace(elementName, currentCount - 1);
                            break;
                        }
                    }
                }
                if(voxelMode) {
                    this.constructVoxels();
                }
                fileString = reader.readLine();
            }
            reader.close();
            return true;
        }
        catch (Exception e){
            return this.fail();
        }
    }

    /**
     * Return result of bynary loadiung
     * @param filename path to file
     * @param endian byte order
     * @return true if success, else - false
     */
    private boolean binaryParsing(String filename, ByteOrder endian) {
        //vertex clous thin coefs
        int analizeThinCoef = 1;
        //elements
        HashMap<String, Integer> elementsCounts = new HashMap<String, Integer>();
        ArrayList<String> elementNames = new ArrayList<String>();
        //properties
        //HashMap<String, HashMap<String, Integer>> propertiesValues = new HashMap<String, HashMap<String, Integer>>();
        HashMap<String, ArrayList<String>> propertiesValues = new HashMap<String, ArrayList<String>>();
        HashMap<String, ArrayList<String>> propertiesTypes = new HashMap<String, ArrayList<String>>();
        //flags
        try {
            RandomAccessFile file = new RandomAccessFile(filename, "r");
            String fileString = file.readLine();
            //header
            while (fileString != null && !fileString.equals("end_header")) {
                String[] stringParts = fileString.split(" ");
                //element
                if(stringParts[0].equals("element")){
                    elementsCounts.put(stringParts[1], Integer.parseInt(stringParts[2]));
                    //propertiesValues.put(stringParts[1], new HashMap<String, Integer>());
                    propertiesValues.put(stringParts[1], new ArrayList<String>());
                    propertiesTypes.put(stringParts[1], new ArrayList<String>());
                    elementNames.add(stringParts[1]);
                }
                //property
                if(stringParts[0].equals("property")){
                    if(stringParts.length <= 3){
                        if(stringParts[1].equals("list")) {
                            propertiesTypes.get(elementNames.get(elementNames.size() - 1)).add(stringParts[1] + " " + stringParts[2]);
                            propertiesValues.get(elementNames.get(elementNames.size() - 1)).add(stringParts[4]);
                        } else {
                            propertiesValues.get(elementNames.get(elementNames.size() - 1)).add(stringParts[2]);
                            propertiesTypes.get(elementNames.get(elementNames.size() - 1)).add(stringParts[1]);
                        }
                    }
                }
                fileString = file.readLine();
            }
            if(!elementsCounts.containsKey("face")){
                analizeThinCoef = (int)(elementsCounts.get("vertex") / maxAnalizeVertex);
            }
            ByteBuffer reader;
            int remain = (int) (file.length() - file.getFilePointer());
            reader = ByteBuffer.allocate(remain);
            reader.order(endian);
            for (int i = 0; i < remain; i++) {
                byte b = file.readByte();
                reader.put(b);
            }
            reader.position(0);
            for (String elementName : elementNames){
                for (int i = 0; i < elementsCounts.get(elementName); i ++){
                    Vertex vertex = new Vertex();
                    for(int j = 0; j < propertiesTypes.get(elementName).size(); j ++){
                        String[] temp = this.getBinaryData(reader, propertiesTypes.get(elementName).get(j));
                        Integer tempInt = !temp[0].equals("NaN")?Integer.parseInt(temp[0]):null;
                        Double tempFloat = !temp[1].equals("NaN")?Double.parseDouble(temp[1]):null;
                        if(elementName.equals("vertex")){
                            switch (propertiesValues.get(elementName).get(j)){
                                case "x": vertex.setX(tempFloat != null?tempFloat:(double)tempInt); break;
                                case "y": vertex.setY(tempFloat != null?tempFloat:(double)tempInt); break;
                                case "z": vertex.setZ(tempFloat != null?tempFloat:(double)tempInt); break;
                                case "red":  vertex.setRed(tempInt); break;
                                case "green": vertex.setGreen(tempInt); break;
                                case "blue": vertex.setBlue(tempInt); break;
                            }
                        }
                    }
                    if(i % analizeThinCoef == 0) {
                        this.loadedVertexCloud.add(vertex);
                    }
                    this.vertexCloudLoaded = true;
                }
            }
            file.close();
            if(elementsCounts.containsKey("face")){
                this.constructVoxels();
            }
            return true;
        }
        catch (Exception e){
            return this.fail();
        }
    }

    /**
     * Return result of parsing bynary sequence
     * @param reader byte buffer
     * @param type type of data
     * @return array of variants of parsing data
     */
    private String[] getBinaryData(ByteBuffer reader, String type) {
        Integer tempInt = null;
        Double tempFloat = null;
        switch (type) {
            case "char": {
                tempInt =  new Integer(reader.get());
            } break;
            case "uchar": {
                tempInt = new Integer(reader.get());
                tempInt = tempInt & 0xff;
            } break;
            case "short": {
                short temp = reader.getShort();
                tempInt = new Integer(temp);
            } break;
            case "ushort": {
                short temp = reader.getShort();
                tempInt = new Integer(temp);
                tempInt = tempInt & 0xff;
            } break;
            case "int":{
                tempInt = reader.getInt();
            } break;
            case "uint":{
                tempInt = reader.getInt();
                tempInt =  tempInt & 0xff;
            } break;
            case "float":{
                tempFloat = new Double(reader.getFloat());
            } break;
            case "double":{
                tempFloat = new Double(reader.getDouble());
            } break;
            default: {
                ;
            }
        }
        String[] result = new String[2];
        result[0] =  tempInt != null?Integer.toString(tempInt):"NaN";
        result[1] =  tempFloat != null?Double.toString(tempFloat):"NaN";
        return result;
    }

    /**
     * Construct voxels array
     */
    private void constructVoxels() {
        for (Vertex vertex : this.loadedVertexCloud) {
            Voxel voxel = new Voxel(vertex, CompareConfig.getPrecision());
            this.loadedVoxelsArray.add(voxel);
        }
        this.voxelsLoaded = true;
    }


}
