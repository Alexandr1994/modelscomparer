import java.util.ArrayList;
import java.util.HashMap;

/**
 * Prepearing model and vertex cloud helper class
 * and contain vertex cloud and 3D-model normalizing
 * parametrs after prepearing
 */
public class Normalizer {

    /**
     * Rotation angle about Oz axis
     */
    private double angle = 0;

    /**
     * Return rotation angle about Oz axis
     * @return rotation angle
     */
    public double getAngle() {
        return angle;
    }

    /**
     * Flag of model or vertex cloud scaling
     * true - model, false - cloud
     */
    private boolean modelScale;

    /**
     * return flag of model or vertex cloud scaling
     * @return true - model, false - cloud
     */
    public boolean IsModelOfCloud() {
        return this.modelScale;
    }

    /**
     * Size scale
     */
    private double scale = 1;

    /**
     * Return size scale
     * @return ize scale
     */
    public double getScale() {
        return scale;
    }

    /**
     * rotation center
     */
    private double centerX; /* x coord */
    private double centerY; /* y coord */

    /**
     * offset center from (0;0;0) by Ox axis
     */
    private double xCenterOffset;

    /**
     * Return center X coordinate
     * @return
     */
    public double getCenterX() {
        return centerX;
    }

    /**
     * offset center from (0;0;0) by Oy axis
     */
    private double yCenterOffset;

    /**
     * Return center Y coordinate
     * @return
     */
    public double getCenterY() {
        return centerY;
    }

    /**
     * Vertex slice of vertex cloud
     */
    private ArrayList<Vertex> vertexSliceCloud;

    /**
     * Vertex slice of model voxel array
     */
    private ArrayList<Vertex> vertexSliceModel;

    /**
     * Construct slice by vertex cloud
     * @param vertexCloud vertex cloud
     * @param sliceZ Z level of slice
     * @param sliceWidth With of slice
     * @return slice
     */
    private ArrayList<Vertex> constructSlicesByVertexes(ArrayList<Vertex> vertexCloud, double sliceZ , double sliceWidth) {
        ArrayList<Vertex> slice = new ArrayList<Vertex>();
        for(Vertex vertex : vertexCloud) {
            if(Math.abs(vertex.getZ() - sliceZ) < sliceWidth) {
                Vertex newVertex = new Vertex();
                newVertex.setX(vertex.getX());
                newVertex.setY(vertex.getY());
                newVertex.setZ(0);
                slice.add(newVertex);
            }
        }
        return slice;
    }

    /**
     * Construct slice by voxel array
     * @param voxelArray voxel array
     * @param sliceZ Z level of slice
     * @param sliceWidth With of slice
     * @return slice
     */
    private ArrayList<Vertex> constructSliceByVoxels(ArrayList<Voxel> voxelArray, double sliceZ , double sliceWidth) {
        ArrayList<Vertex> slice = new ArrayList<Vertex>();
        for(Voxel voxel : voxelArray) {
            Vertex vertex = voxel.getCenter();
            if(Math.abs(vertex.getZ() - sliceZ) <= sliceWidth) {
                Vertex newVertex = new Vertex();
                newVertex.setX(vertex.getX());
                newVertex.setY(vertex.getY());
                newVertex.setZ(0);
                slice.add(newVertex);
            }
        }
        return slice;
    }

    /**
     * Scaling (using scale and rotation correction coefficients)
     * @param cloudRadius radius of thined vertex cloud
     * @see CompareConfig
     */
    private void scaling(double cloudRadius) {
        /* sizes of slices*/
        HashMap<String, Double> modelBorders = this.sliceBorders(this.vertexSliceModel);
        double modelDiagonal = Math.sqrt(Math.pow(modelBorders.get("max_x") - modelBorders.get("min_x"), 2) +
                Math.pow(modelBorders.get("max_y") - modelBorders.get("min_y"), 2));
        double coef = 1;
        /* scaling */
        if(modelDiagonal > 2 * cloudRadius) {
            this.scale = coef = (2 * cloudRadius) / modelDiagonal;
            this.scale *= CompareConfig.getScaleCoef();
            vertexSliceModel = this.sliceScale(vertexSliceModel, CompareConfig.getRotateScaleCoef() * coef);
            this.modelScale = true;
        } else if(modelDiagonal < 2 * cloudRadius) {
            this.scale = coef = modelDiagonal / (2 * cloudRadius);
            this.scale *= CompareConfig.getScaleCoef();
            vertexSliceCloud = this.sliceScale(vertexSliceCloud, CompareConfig.getRotateScaleCoef() * coef);
            this.modelScale = false;
        }
    }

    /**
     * Combination slices centers
     */
    private void centering() {
        /* centering */
        HashMap<String, Double> cloudBorders = this.sliceBorders(vertexSliceCloud);
        HashMap<String, Double> modelBorders = this.sliceBorders(vertexSliceModel);
        /* cloud slice offsets */
        double x0 = ((cloudBorders.get("max_x") + cloudBorders.get("min_x")) / 2);
        double y0 = ((cloudBorders.get("max_y") + cloudBorders.get("min_y")) / 2);
        /* voxels slice offsets */
        double x1 = ((modelBorders.get("max_x") + modelBorders.get("min_x")) / 2);
        double y1 = ((modelBorders.get("max_y") + modelBorders.get("min_y")) / 2);
        /* cloud slice offseting */
        for(Vertex vertex : vertexSliceCloud) {
            vertex.setX(vertex.getX() - x0);
            vertex.setY(vertex.getY() - y0);
        }
        /* voxels slice offseting */
        for(Vertex vertex : vertexSliceModel) {
            vertex.setX(vertex.getX() - x1);
            vertex.setY(vertex.getY() - y1);
        }
        this.centerX = 0;
        this.centerY = 0;
    }

    /**
     * Finding rotation about Oz axis angle by maximum slice match
     */
    private void angleSearching() {
        int maxCount = 0;
        double angle = 0;
        while (angle < 2 * Math.PI) {
            ArrayList<Vertex> cloudTest = this.sliceCopy(vertexSliceCloud);
            ArrayList<Vertex> modelTest = this.sliceCopy(vertexSliceModel);
            /* rotating */
            for (Vertex vertex : cloudTest) {
                vertex.rotateZ(angle, this.centerX, this.centerY);
            }
            /* checking */
            int count = 0;
            for(Vertex cloudVertex : cloudTest) {
                for(Vertex modelVertex : modelTest ) {
                    if(cloudVertex.getDistanceTo(modelVertex) < 0.1) {
                        count ++;
                    }
                }
            }
            if (count >= maxCount) {
                maxCount = count;
                this.angle = angle;
            }
            angle += 5 * Math.PI / 180;
        }
    }

    /**
     * Preperaoing
     * @param cloudContainer vertex cloud 3D-object container
     * @param modelContainer model 3D-object container
     * @param cloudRadius thined vertex cloud radius
     */
    public void normalazing(Container3D cloudContainer, Container3D modelContainer, double cloudRadius) {
        /* constructing slices */
        this.vertexSliceCloud = new ArrayList<Vertex>();
        this.vertexSliceModel = new ArrayList<Vertex>();
        HashMap<String, Double> cloudBorders = cloudContainer.getBorderValues();
        /* slicing vertex cloud */
        this.vertexSliceCloud = this.constructSlicesByVertexes(cloudContainer.getLoadedVertexCloud(),
                                                        0.1,
                                                     0.1 * Math.abs((cloudBorders.get("max_z") - cloudBorders.get("min_z"))));
        /* slicing model cloud */
        HashMap<String, Double> borderValuesModel = modelContainer.getBorderValues();
        this.vertexSliceModel = this.constructSliceByVoxels(modelContainer.getLoadedVoxelArray(),
                                                     (borderValuesModel.get("max_z") + borderValuesModel.get("min_z")) / 2,
                                                  0.9 * Math.abs((borderValuesModel.get("max_z") - borderValuesModel.get("min_z"))));
        /* scaling */
        this.scaling(cloudRadius);
        /* centering */
        this.centering();
        /* thining */
        this.vertexSliceCloud = this.thin(this.vertexSliceCloud, CompareConfig.getPrecision() / 2);
        this.vertexSliceModel = this.thin(this.vertexSliceModel, CompareConfig.getPrecision());
        /* angle searching */
        this.angleSearching();
    }

    /**
     * Return thined slice
     * @param slice
     * @param precision
     * @return thined slice
     */
    private ArrayList<Vertex> thin(ArrayList<Vertex> slice, double precision) {
        ArrayList<Vertex> newSlice = new ArrayList<Vertex>();
        for(int i = 0; i < slice.size(); i ++) {
            if(slice.get(i).isActive()) {
                newSlice.add(slice.get(i));
                for (int j = i + 1; j < slice.size(); j++) {
                    if (!slice.get(j).isActive()) {
                        continue;
                    }
                    if (slice.get(i).getDistanceTo(slice.get(j)) < precision) {
                        slice.get(j).deactive();
                    }
                }
            }
        }
        return newSlice;
    }

    /**
     * Return scaled slice
     * @param slice
     * @param coef
     * @return scaled slice
     */
    private ArrayList<Vertex> sliceScale(ArrayList<Vertex> slice, double coef) {
        for (Vertex vertex : slice) {
            vertex.scale(coef, coef, coef);
        }
        return slice;
    }

    /**
     * Return new object
     * @param slice
     * @return copy of slice
     */
    private ArrayList<Vertex> sliceCopy(ArrayList<Vertex> slice) {
        ArrayList<Vertex> copy = new ArrayList<Vertex>();
        for (Vertex vertex : slice) {
            Vertex newVertex = new Vertex();
            newVertex.setX(vertex.getX());
            newVertex.setY(vertex.getY());
            newVertex.setZ(vertex.getZ());
            copy.add(newVertex);
        }
        return copy;
    }

    /**
     * Return border values of slice
     * @param slice
     * @return borer vkuse of slice
     */
    private HashMap<String, Double> sliceBorders(ArrayList<Vertex> slice) {
        HashMap<String, Double> borderValues = new HashMap<String, Double>();
        //X
        borderValues.put("min_x", slice.get(0).getX());
        borderValues.put("max_x", slice.get(0).getX());
        //Y
        borderValues.put("min_y", slice.get(0).getY());
        borderValues.put("max_y", slice.get(0).getY());
        for(Vertex vertex : slice) {
            //X
            if(vertex.getX() < borderValues.get("min_x")) {
                borderValues.replace("min_x", vertex.getX());
            }
            if(vertex.getX() > borderValues.get("max_x")) {
                borderValues.replace("max_x", vertex.getX());
            }
            //Y
            if(vertex.getY() < borderValues.get("min_y")) {
                borderValues.replace("min_y", vertex.getY());
            }
            if(vertex.getY() > borderValues.get("max_y")) {
                borderValues.replace("max_y", vertex.getY());
            }
        }
        return borderValues;
    }

    /**
     * Return primary scale coefficient
     * @param cloudContainer vertex cloud 3D-object containet
     * @param modelContainer model 3D-object container
     * @return scale coefficient
     */
    public double primaryModelScaling(Container3D cloudContainer, Container3D modelContainer) {
        HashMap<String, Double> borderValuesCloud = cloudContainer.getBorderValues();
        HashMap<String, Double> borderValuesModel = modelContainer.getBorderValues();
        double modelDiagonal = Math.sqrt(Math.pow(borderValuesModel.get("max_x") - borderValuesModel.get("min_x"), 2) +
                                         Math.pow(borderValuesModel.get("max_y") - borderValuesModel.get("min_y"), 2) +
                                         Math.pow(borderValuesModel.get("max_z") - borderValuesModel.get("min_z"), 2));

        double cloudDiagonal = Math.sqrt(Math.pow(borderValuesCloud.get("max_x") - borderValuesCloud.get("min_x"), 2) +
                                         Math.pow(borderValuesCloud.get("max_y") - borderValuesCloud.get("min_y"), 2) +
                                         Math.pow(borderValuesCloud.get("max_z") - borderValuesCloud.get("min_z"), 2));

        double scale = cloudDiagonal / modelDiagonal;
        return scale;
    }

}
