/**
 * Class Voxel 3D-pixel
 */
public class Voxel {

    /**
     * Center of voxel
     */
    private Vertex center;

    /**
     * Return center of voxel
     * @return center of voxel
     */
    public Vertex getCenter() {
        return this.center;
    }

    /**
     * Edge size (precision)
     */
    private double edge;

    /**
     * Return edge size
     * @return edge
     */
    public double getEdge() {
        return this.edge;
    }

    /**
     * Inner within vertexes count (hitted vertexes)
     */
    private int vertexCounter;

    /**
     * Return count of within vertex
     * @return countof within vertex
     */
    public int getVertexCounter() {
        return this.vertexCounter;
    }

    /**
     * Return true if exists at least one vertex within voxel, else - false
     * @return true - if has vertexes, else - false
     */
    public boolean hasVertexes() {
        if(this.vertexCounter > 0) {
            return true;
        }
        return false;
    }

    /**
     * Reset innser vertex counter
     */
    public void resetVertexCounter() {
        this.vertexCounter = 0;
    }

    /**
     * Constructor of class
     * @param center voxel center coords
     * @param edge size of edge
     */
    public Voxel(Vertex center, double edge) {
        this.center = center;
        this.edge = edge;
        this.vertexCounter = 0;
    }

    /**
     * Check laying vertex within voxel
     * @param vertex
     * @return true if vertex laying within voxel, else - false
     */
    public boolean checkVertex(Vertex vertex) {
        if(vertex != null &&
                Math.abs(this.center.getX() - vertex.getX()) <= this.edge / 2 &&
                Math.abs(this.center.getY() - vertex.getY()) <= this.edge / 2 &&
                Math.abs(this.center.getZ() - vertex.getZ()) <= this.edge / 2) {
            this.vertexCounter ++;
            return true;
        }
        return false;
    }

    /**
     * Offset voxel center
     * @param xDist
     * @param yDist
     * @param zDist
     */
    public void centerMerge(double xDist, double yDist, double zDist) {
        this.center.setX(this.center.getX() + xDist);
        this.center.setY(this.center.getY() + yDist);
        this.center.setZ(this.center.getZ() + zDist);
    }

}