import javax.print.DocFlavor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Parser of stl files extends abstract class parser
 */
public class StlParser extends Parser {

    /**
     * Load vertex cloud and model from file
     * @param filename path to file
     * @return true if success, else - false
     */
    public boolean load(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String headLine = reader.readLine();
            headLine = headLine.split(" ")[0];
            reader.close();
            if(headLine.equals("solid")) {
                return this.asciiParsing(filename);
            }
            return this.binaryParsing(filename);
        }
        catch (Exception e) {
            return this.fail();
        }
    }

    /**
     * Return result of ascii loading
     * @param filename path to file
     * @return true if success, else - false
     */
    private boolean asciiParsing(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            while (!line.split(" ")[0].equals("endsolid")) {
                if(line.split(" ")[0].equals("facet")) {
                    Face face = new Face();
                    //line = reader.readLine();//normals
                    while(!line.equals("endfacet")) {//vertexes
                        line = reader.readLine();
                        String[] lineParts = line.split(" ");
                        if(lineParts[0].equals("vertex")) {
                            Vertex vertex = new Vertex();
                            vertex.setX(Double.parseDouble(lineParts[lineParts.length - 3]));
                            vertex.setY(Double.parseDouble(lineParts[lineParts.length - 2]));
                            vertex.setZ(Double.parseDouble(lineParts[lineParts.length - 1]));
                            this.loadedVertexCloud.add(vertex);
                            face.addVertex(vertex);
                        }
                    }
                    this.loadedModel.addFace(face);
                }
                line = reader.readLine();
            }
            reader.close();
            if(this.loadedModel.getFacesCount() < maxFacesCount) {
                this.constructVoxels();
                this.loadedModel = null;
            } else {
                this.modelLoaded = true;
            }
            this.vertexCloudLoaded = true;
            return true;
        }
        catch (Exception e) {
            return this.fail();
        }
    }

    /**
     * Return result of bynary loadiung
     * @param filename path to file
     * @return true if success, else - false
     */
    private boolean binaryParsing(String filename) {
        try {
            boolean voxelsMode = false;
            RandomAccessFile file = new RandomAccessFile(filename, "r");
            //creating byte buffer for endian
            ByteBuffer buffer = ByteBuffer.allocate((int)file.length());
            for(int i = 0; i < file.length(); i ++) {
                buffer.put(file.readByte());
            }
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.position(80);
            //faces-triangles count in model
            int facesCount = buffer.getInt();
            if(facesCount > maxFacesCount) {
                voxelsMode = true;
            }

            for(int i = 0; i < facesCount; i ++) {
                if(!voxelsMode) {
                    //face-triangle
                    Face face = new Face();
                    //normal
                    buffer.getFloat();
                    buffer.getFloat();
                    buffer.getFloat();
                    //vertex1
                    Vertex vertex1 = new Vertex();
                    vertex1.setX(buffer.getFloat());
                    vertex1.setY(buffer.getFloat());
                    vertex1.setZ(buffer.getFloat());
                    this.loadedVertexCloud.add(vertex1);
                    face.addVertex(vertex1);
                    //vartex2
                    Vertex vertex2 = new Vertex();
                    vertex2.setX(buffer.getFloat());
                    vertex2.setY(buffer.getFloat());
                    vertex2.setZ(buffer.getFloat());
                    this.loadedVertexCloud.add(vertex2);
                    face.addVertex(vertex2);
                    //vertex3
                    Vertex vertex3 = new Vertex();
                    vertex3.setX(buffer.getFloat());
                    vertex3.setY(buffer.getFloat());
                    vertex3.setZ(buffer.getFloat());
                    this.loadedVertexCloud.add(vertex3);
                    face.addVertex(vertex3);
                    //face end
                    buffer.getChar();
                    this.loadedModel.addFace(face);
                } else {
                    Vertex vertex = new Vertex();
                    for(int j = 0; j < 4; j ++) {
                        vertex.setX(buffer.getFloat());
                        vertex.setY(buffer.getFloat());
                        vertex.setZ(buffer.getFloat());
                    }
                    this.loadedVertexCloud.add(vertex);
                    buffer.getChar();
                }
            }
            file.close();
            if(voxelsMode) {
                this.constructVoxels();
                this.loadedModel = null;
            } else {
                this.modelLoaded = true;
            }
            this.vertexCloudLoaded = true;
            return true;
        }
        catch (Exception e) {
            return this.fail();
        }
    }

    /**
     * Construct voxels array
     */
    private void constructVoxels() {
        //constructiong voxel array
        for (Vertex vertex : this.loadedVertexCloud) {
            Voxel voxel = new Voxel(vertex, CompareConfig.getPrecision());
            this.loadedVoxelsArray.add(voxel);
        }
        this.voxelsLoaded = true;
    }


}
