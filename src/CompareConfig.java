import java.util.HashMap;

/**
 * Class CompareConfig contains static corrective
 * thining, scaling, rotating coefficients
 * and analyze precision, which may be set
 * by addition file of outer configuration
 */
public class CompareConfig {

    /**
     * Vertex cloud thinning coefficient
     */
    private static double thinCoef;

    /**
     * Scaling coefficient
     */
    private static double scaleCoef;

    /**
     * Rotation about Oz axis coefficient
     */
    private static double rotateScaleCoef;

    /**
     * Precision of analyze (size of edge of voxel)
     */
    private static double precision;

    /**
     * Return loaded of default vertex cloud thinning coefficient
     * @return vertex cloud thinning coefficient
     */
    public static double getThinCoef() {
        return thinCoef;
    }

    /**
     * Return loaded of default scaling coefficient
     * @return scaling coefficient
     */
    public static double getScaleCoef() {
        return scaleCoef;
    }

    /**
     * Return loaded of default rotation about Oz axis coefficient
     * @return rotating coefficient
     */
    public static double getRotateScaleCoef() {
        return rotateScaleCoef;
    }

    /**
     * Return loaded of default precision or size of analyzing voxel's edge
     * @return precision
     */
    public static double getPrecision() { return  precision; }

    /**
     * Load corrective coefficient from configuration file
     * and setting them in programm configuration if they
     * containing in loading file
     * <p>
     * If outer file not contain some data it will evaluates
     * by default
     * @param fileName path to outer configuration file
     */
    public static void loadCoefs(String fileName) {
        HashMap<String, Double> outerConfig = InputOutput.loadOuterConfig(fileName);
        /* thin coef */
        thinCoef = 1;
        if(outerConfig.containsKey("thin_coef")) {
            if(outerConfig.get("thin_coef") > 0 && outerConfig.get("thin_coef") < 5) {
                thinCoef = outerConfig.get("thin_coef");
            }
        }
        /* scale coef */
        scaleCoef = 1;
        if(outerConfig.containsKey("scale_coef")) {
            if(outerConfig.get("scale_coef") > 0 && outerConfig.get("scale_coef") < 3) {
                scaleCoef = outerConfig.get("scale_coef");
            }
        }
        /* rotation scale coef */
        rotateScaleCoef = 1;
        if(outerConfig.containsKey("rotate_scale_coef")) {
            if(outerConfig.get("rotate_scale_coef") > 0 && outerConfig.get("rotate_scale_coef") < 3) {
                rotateScaleCoef = outerConfig.get("rotate_scale_coef");
            }
        }
        /* precision */
        precision = 0.1;
        if(outerConfig.containsKey("precision")) {
            if(outerConfig.get("precision") > 0 && outerConfig.get("precision") < 1) {
                precision = outerConfig.get("precision");
            }
        }
    }
}
