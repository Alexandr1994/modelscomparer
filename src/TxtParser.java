import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Parser of txt files (only vertex colouds) extends abstract class parser
 */
public class TxtParser extends Parser{

    /**
     * Load vertex cloud from file
     * @param filename path to file
     * @return true if success, else - false
     */
    public boolean load(String filename) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            String line = reader.readLine();
            while (line != null) {
                Vertex vertex = new Vertex();
                String[] parts = line.split(" ");
                String[] coords = new String[parts.length];
                int index = 0;
                for(int i = 0; i < parts.length; i ++) {
                    if(!parts[i].equals("")) {
                        coords[index] = parts[i];
                        index ++ ;
                    }
                }
                vertex.setX(Double.parseDouble(coords[0]));
                vertex.setY(Double.parseDouble(coords[1]));
                vertex.setZ(Double.parseDouble(coords[2]));
                Voxel voxel = new Voxel(vertex, CompareConfig.getPrecision());
                this.loadedVertexCloud.add(vertex);
                this.loadedVoxelsArray.add(voxel);
                line = reader.readLine();
            }
            this.voxelsLoaded = true;
            this.vertexCloudLoaded = true;
            return true;
        }
        catch(Exception e) {
            return  this.fail();
        }
    }
}
