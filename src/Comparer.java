import com.threed.jpct.Config;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class Comparer complete comparing analyzeof 3D-model and vertex cloud,
 * creating report and starting visualization of results of analize
 */
public class Comparer {

    /**
     * Path to report file
     */
    private String reportPath;

    /**
     * Presition of analyze
     */
    private double precision = 0.1;

    /**
     * Visualization scale
     */
    private float viewScale = 5;

    /**
     * Inner counter of vertexes in cloud
     */
    private int vertexCounter = 0;

    /**
     * Container of loading 3d-model
     */
    private Container3D modelContainer;

    /**
     * Container of laoding vertex cloud
     */
    private Container3D cloudContainer;

    /**
     * Comparing method
     * <p>
     * Can finish programm with exit code -2
     * if inputed files not exist or not valid
     * @param modelFilePath path to file of 3d-model
     * @param cloudFilePath path to file of vertex cloud
     * @param reportPath path to reports folder
     */
    public void Compare(String modelFilePath, String cloudFilePath, String reportPath) {
        this.reportPath = reportPath;
        /* loading model */
        this.modelContainer = InputOutput.loadModel(modelFilePath);
        if(this.modelContainer == null) {
            System.out.print("Ошибка файла модели!");
            System.exit(-2);
        }
        /* loading vertex cloud */
        this.cloudContainer = InputOutput.loadVertexCloud(cloudFilePath);
        if(this.cloudContainer == null){
            System.out.print("Ошибка файла облака точек!");
            System.exit(-2);
        }
        /* preparing for comparing analyze */
        double radius = this.cloudContainer.thinVertexCloud(); /* removeing extra vertexes */
        this.prepearing(radius);
        /* analyzing */
        if(this.voxelAnalyze(this.cloudContainer.getLoadedVertexCloud())) {
            /* visualisation */
            this.visualization();
        }
    }

    /**
     * Construct and show visualization of result of comparing analyze, simplifed
     * 3D-model and thinde vertex array
     * <p>
     * Can finish programm with exit code -3
     */
    private void visualization() {
        try {
            /* confugurating graphic library for mutlithreading and multiwindow displaing */
            Config.useMultipleThreads = true;
            Config.useMultiThreadedBlitting = true;
            Config.mtDebug = true;
            Config.loadBalancingStrategy = 1;
            /* uploading of texture */
            TextureManager.getInstance().addTexture("texture", new Texture("texture.jpg"));
            /* Visualization of analyze result */
            View view = new View("Результат проверки");
            HashMap<String, Double> borderValuesV = this.cloudContainer.getBorderValues();
            HashMap<String, Double> borderValuesM = this.modelContainer.getBorderValues();
            view.setViewCenter(0,0,0);
            view.loadVoxelsArray(this.modelContainer.getLoadedVoxelArray(), this.viewScale);
            view.loadVertexsArray(this.cloudContainer.getLoadedVertexCloud(), this.viewScale);
            /* visualization of 3D-model  */
            if(this.modelContainer.isModelLoaded()) {
                /* as polygonal model */
                View modelView = new View("Эталонная модель");
                modelView.setViewCenter(0,0,0);
                modelView.loadFacesArray(this.modelContainer.getLoadedModel().getFaces(), this.viewScale);
                Thread cloud = new Thread(modelView);
                cloud.start();
            } else {
                /* as voxel array */
                View modelView = new View("Эталонная модель");
                modelView.setViewCenter(0,0,0);
                ArrayList<Vertex> vertices = new ArrayList<Vertex>();
                for(Voxel voxel: this.modelContainer.getLoadedVoxelArray()) {
                    vertices.add(voxel.getCenter());
                }
                modelView.loadVertexsArray(vertices, this.viewScale);
                Thread cloud = new Thread(modelView);
                cloud.start();
            }
            /* vertex cloud */
            if(this.cloudContainer.isVertexCloudLoaded()) {
                View cloudView = new View("Облако точек");
                cloudView.setViewCenter(0,0,0);
                cloudView.loadVertexsArray(this.cloudContainer.getLoadedVertexCloud(), this.viewScale);
                Thread faces = new Thread(cloudView);
                faces.start();
            }
            Thread voxels = new Thread(view);
            voxels.start();
        }
        catch (Exception e) {
            System.out.print("Ошибка визуализации!");
            System.exit(-3);
        }
    }

    /**
     * Complete checking of hitting of all vertexes from cloud
     * in one of voxels (checking of "filling" of voxels)
     * <p>
     * If at least one vertex has hit in voxel this voxel will considered "filled"
     * @param vertexCloud vertex cloud
     */
    private void vertexAnalyze(ArrayList<Vertex> vertexCloud) {
        for (Vertex vertex: vertexCloud) {
            for (Voxel voxel: this.modelContainer.getLoadedVoxelArray()) {
                if(voxel.checkVertex(vertex)) {
                    this.vertexCounter++;
                }
            }
        }
    }

    /**
     * Complete checking of "filling" of voxels arrray
     * and calculation of equation persent of vertex cloud
     * and statistic parametrs from count of "filled" voxels
     * <p>
     * If 3D-model was loaded as polygonal, then will be calculated parameter by polygon
     * "Filling" of polygon based on count of "filled" voxels located within of this polygon
     * <p>
     * Voxel is considered "located within of polygon" if point of proection of its center on this polygon
     * located within of voxel (proection point hit in voxel)
     * @param vertexCloud vertex cloud
     * @return true - success, false - error of data
     */
    private boolean voxelAnalyze(ArrayList<Vertex> vertexCloud) {
        ArrayList<Double> facePersents = new ArrayList<Double>();
        double summ = 0;
        double disp = 0;
        /* find "filling" of voxel array */
        this.vertexAnalyze(vertexCloud);
        int fullVoxelsCounter = 0;
        for (Voxel voxel: this.modelContainer.getLoadedVoxelArray()) {
            if(voxel.hasVertexes()) {
                fullVoxelsCounter ++;
                /* if model loaded as polygonal */
                if(this.modelContainer.isModelLoaded()) {
                    /* find "fillng"  voxels of polygon (polygonal analyze) */
                    for (Face face : this.modelContainer.getLoadedModel().getFaces()) {
                        if (face.isActive() && face.getVoxelsIndexes().contains(this.modelContainer.getLoadedVoxelArray().indexOf(voxel))) {
                            face.addFilledVoxel();
                        }
                    }
                }
            }
        }
        /* if model loaded as ploygonal */
        if(this.modelContainer.isModelLoaded()) {
            /* find statistic by polygons  */
            for (Face face : this.modelContainer.getLoadedModel().getFaces()) {
                if (!face.isActive()) {
                    continue;
                }
                double customPersent = face.getVoxelsIndexes().size() != 0 ? ((double) face.getFilledVoxelsCount()) / ((double) face.getVoxelsIndexes().size()) : 0;
                facePersents.add(customPersent);
                summ += customPersent;
            }
            summ /= facePersents.size();
            for (Double persent : facePersents) {
                double customDisp = Math.pow((persent - summ), 2);
                disp += customDisp;
            }
            disp /= facePersents.size();
        }
        /* calculation of "filling" percent of voxel array */
        double persent = (double) fullVoxelsCounter / (double) this.modelContainer.getLoadedVoxelArray().size();
        /* creating report file */
        return this.doReport(persent, facePersents, summ, disp);
    }

    /**
     * Constructiong report with results of comparing analyze (In russian language)
     * @param commonPersent persent of "filling" voxel array
     * @param facePersents Persents of "filling" voxels of all polygons of model (polygonal analyze)
     * @param customSumm statistic mathematical expectation of "filling" of polygons (polygonal analyze)
     * @param disp statistic dispersion "filling" of polygons (polygonal analyze)
     * @return true - if file report war created, else false
     */
    private boolean doReport(double commonPersent, ArrayList<Double> facePersents, double customSumm, double disp) {
        String reportString = "";
        reportString += "ОСНОВНАЯ ОЦЕНКА:";
        reportString += "\n";
        reportString += "\nОбщий процент соответствия облака точек модели: " + Double.toString(commonPersent * 100) + "%.";
        reportString += "\n";
        reportString += "\nДОПОЛНИТЕЛЬНАЯ ОЦЕНКА:";
        reportString += "\n";
        reportString += "\nСредний процент соответствия по полигонам модели (МО): " + Double.toString(customSumm * 100) + "%.";
        reportString += "\nОтклонение процента соответствия по полигонам модели (СКО): " + Double.toString(disp * 100) + "%.";
        reportString += "\n";
        reportString += "\nДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ:";
        reportString += "\n";
        reportString += "\nРазмерность анализируемого облака точек: " + Integer.toString(this.cloudContainer.getLoadedVertexCloud().size()) + ".";
        if(this.modelContainer.isModelLoaded()) {
            reportString += "\nКоличество полигонов модели: " + Integer.toString(this.modelContainer.getLoadedModel().getFacesCount()) + ".";
        }
        reportString += "\nТочность анализа: " + Double.toString(this.precision) + ".";
        return InputOutput.savingReport(this.reportPath + (System.currentTimeMillis() / 1000) + "_report.txt", reportString);
    }

    /**
     * Prepearing vertex cloud to analyze
     * @param cloudRadius Radius of sphere region of vertex cloud after trimming on "noises"
     */
    private void prepearing(double cloudRadius) {
        Normalizer normalizer = new Normalizer();
        /* scaling */
        double primaryScale = normalizer.primaryModelScaling(this.cloudContainer, this.modelContainer);
        this.modelContainer.scale(primaryScale, primaryScale, primaryScale);
        if(!this.modelContainer.isVoxelsLoaded()) {
            this.modelContainer.constructVoxels();
        }
        this.modelContainer.constructBorderValues();
        /* positions combination */
        this.centersMerge();
        normalizer.normalazing(this.cloudContainer, this.modelContainer, cloudRadius);
        if(normalizer.IsModelOfCloud()) {
            this.modelContainer.scale(normalizer.getScale(), normalizer.getScale() * 0.75, normalizer.getScale());
        } else {
            this.cloudContainer.scale(normalizer.getScale() , normalizer.getScale(), normalizer.getScale());
        }
        for(Vertex vertex : this.cloudContainer.getLoadedVertexCloud()) {
            vertex.rotateZ(normalizer.getAngle(), 0, 0);
        }
        /* deleting extra vertex from cloud(mininmalazing of expressinve density)*/
        this.thinVertexes();
        /* deleting extra voxels from voxels array(mininmalazing of expressinve density)  */
        this.thinVoxels();
    }

    /**
     * Thining voxel array
     * <p>
     * Deleting voxels located nearer than setted precision
     */
    private void thinVoxels() {
        /* construction new thined voxel array */
        ArrayList<Voxel> newVoxelArray = new ArrayList<Voxel>();
        for (int i = 0; i < this.modelContainer.loadedVoxelsArray.size(); i++) {
            if (this.modelContainer.loadedVoxelsArray.get(i).getCenter().isActive()) {
                newVoxelArray.add(this.modelContainer.loadedVoxelsArray.get(i));
                for (int j = i + 1; j < this.modelContainer.loadedVoxelsArray.size(); j++) {
                    if (!this.modelContainer.loadedVoxelsArray.get(j).getCenter().isActive()) {
                        continue;
                    }
                    if (this.modelContainer.loadedVoxelsArray.get(i).getCenter().getDistanceTo(this.modelContainer.loadedVoxelsArray.get(j).getCenter()) < CompareConfig.getPrecision()) {
                        this.modelContainer.loadedVoxelsArray.get(j).getCenter().deactive();
                    }
                }
            }
        }
        /* replased old voxel array on thined voxel array */
        this.modelContainer.uploadVoxelsArray(newVoxelArray);
    }

    /**
     * Thining vertex cloud
     * <p>
     * Deleting vertex located nearer than half of setted precision
     */
    private void thinVertexes() {
        /* construction new thined vertex cloud */
        ArrayList<Vertex> newVertexArray = new ArrayList<Vertex>();
        for (int i = 0; i < this.cloudContainer.getLoadedVertexCloud().size(); i++) {
            if (this.cloudContainer.getLoadedVertexCloud().get(i).isActive()) {
                newVertexArray.add(this.cloudContainer.getLoadedVertexCloud().get(i));
                for (int j = i + 1; j < this.cloudContainer.getLoadedVertexCloud().size(); j++) {
                    if (!this.cloudContainer.getLoadedVertexCloud().get(j).isActive()) {
                        continue;
                    }
                    if (this.cloudContainer.getLoadedVertexCloud().get(i).getDistanceTo(this.cloudContainer.getLoadedVertexCloud().get(j)) < CompareConfig.getPrecision() / 2) {
                        this.cloudContainer.getLoadedVertexCloud().get(j).deactive();
                    }
                }
            }
        }
        /* replased old vertex cloud on thined vertex cloud */
        this.modelContainer.uploadVertexCloud(newVertexArray);
    }


    //merging centers of 3D model and vertex cloud

    /**
     * Combination centers of vertex cloud and voxel array in point (0;0;0)
     */
    private void centersMerge() {
        /* centering */
        this.cloudContainer.constructBorderValues();
        HashMap<String, Double> cloudBorders = this.cloudContainer.getBorderValues();
        this.modelContainer.constructBorderValues();
        HashMap<String, Double> modelBorders = this.modelContainer.getBorderValues();
        /* calculation vertex cloud center offset from (0;0;0) */
        double x0 = ((cloudBorders.get("max_x") + cloudBorders.get("min_x")) / 2);
        double y0 = ((cloudBorders.get("max_y") + cloudBorders.get("min_y")) / 2);
        double z0 = ((cloudBorders.get("max_z") + cloudBorders.get("min_z")) / 2);
        /* calculation voxel array center offset from (0;0;0) */
        double x1 = ((modelBorders.get("max_x") + modelBorders.get("min_x")) / 2);
        double y1 = ((modelBorders.get("max_y") + modelBorders.get("min_y")) / 2);
        double z1 = ((modelBorders.get("max_z") + modelBorders.get("min_z")) / 2);
        /* offseting vertex cloud */
        for(Vertex vertex :  this.cloudContainer.getLoadedVertexCloud()) {
            vertex.setX(vertex.getX() - x0);
            vertex.setY(vertex.getY() - y0);
            vertex.setZ(vertex.getZ() - z0);
        }
        /* offseting voxel array */
        for(Voxel voxel : this.modelContainer.getLoadedVoxelArray()) {
            voxel.centerMerge(-x1, -y1, -z1);
        }

    }

}
