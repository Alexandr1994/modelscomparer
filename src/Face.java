import java.util.ArrayList;

/**
 * Polygon of 3D-model
 * <p>
 * Set of vertex (minimal count 3)
 * and set of parametrs of analitic expression of face in geometric space
 */
public class Face {

    /**
     * Face status
     * <p>
     * Not active face will not take part in next actions
     * <p>
     * True - active, else - false
     */
    private boolean active = true;

    /**
     * Face status
     * <p>
     * Not active face will not take part in next actions
     * @return true - active, else - false
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * Set of vertexes of face
     */
    private ArrayList<Vertex> vertexes;

    /**
     * Return set of vertexes of fase
     * @return array of vertexes
     */
    public ArrayList<Vertex> getVertexes() {
        return this.vertexes;
    }

    /**
     * Count of vertexes of face
     */
    private int vertexCount;

    /**
     * Return count of vertexes of face
     * @return count of vertexes of face
     */
    public int getVertexCount() {
        return this.vertexCount;
    }

    /**
     * Inner counter of "full" voxels of face
     * <p>
     * Voxel is considered "located within of face" if point of proection of its center on this face
     * <p>
     * If at least one vertex has hit in voxel this voxel will considered "filled"
     * located within of voxel (proection point hit in voxel)
     */
    private int filledVoxelsCounter;

    /**
     * Increment innet counter of "filled" voxels of face
     */
    public void addFilledVoxel() {
        this.filledVoxelsCounter++;
    }

    /**
     * Retrun count of "filled" voxels of face
     * @return count of "filled" voxels
     */
    public int getFilledVoxelsCount() {
        return this.filledVoxelsCounter;
    }

    /**
     * Set of indexes of voxels located within face
     */
    private ArrayList<Integer> voxelsIndexes;

    /**
     * Return set of indexes of voxels located within face
     * @return set of indexes of voxels
     */
    public ArrayList<Integer> getVoxelsIndexes() {
        return this.voxelsIndexes;
    }

    /**
     * Add new index of voxel
     * @param index index of new voxel
     */
    public void addVoxel(int index) {
        this.voxelsIndexes.add(index);
    }

    /**
     * math params
     */
    private double A;
    private double B;
    private double C;
    private double D = 0;

    /**
     * Constructior of class
     */
    public Face() {
        this.voxelsIndexes =  new ArrayList<>();
        this.vertexes = new ArrayList<>();
        this.vertexCount = 0;
        this.filledVoxelsCounter = 0;
    }

    /**
     * Add new vertex to face
     * @param newVertex new vertex
     */
    public void addVertex(Vertex newVertex){
        this.vertexes.add(newVertex);
        this.vertexCount ++;
    }

    /**
     * Remove vertex with index from face
     * @param index index of vertex
     * @return true - success, false - not exists vertex with that index
     */
    public boolean removeVertex(int index) {
        if(index >= this.vertexCount) {
            return false;
        }
        this.vertexes.remove(index);
        this.vertexCount --;
        return true;
    }

    /**
     * Value of surface function of face
     * @param point coords of point
     * @return value of function (if it = 0 then point is laying on face )
     */
    private double surfaceValue(Vertex point) {
        return this.A * point.getX() + this.B * point.getY() + this.C * point.getZ() + this.D;
    }

    /**
     * Find mathematical parametrs of face
     * @return true success, else - false
     */
    public boolean findMathParams() {
        if(this.vertexCount < 3) {
            this.active = false;
            return false;
        }
        int index = 0;
        boolean right = false;
        while(!right){
            right = true;
            double dx1 = this.vertexes.get((index + 1) % this.vertexCount).getX() - this.vertexes.get(index).getX();
            double dy1 = this.vertexes.get((index + 1) % this.vertexCount).getY() - this.vertexes.get(index).getY();
            double dz1 = this.vertexes.get((index + 1) % this.vertexCount).getZ() - this.vertexes.get(index).getZ();
            double dx2 = this.vertexes.get((index + 2) % this.vertexCount).getX() - this.vertexes.get(index).getX();
            double dy2 = this.vertexes.get((index + 2) % this.vertexCount).getY() - this.vertexes.get(index).getY();
            double dz2 = this.vertexes.get((index + 2) % this.vertexCount).getZ() - this.vertexes.get(index).getZ();
            this.A = (dy1 * dz2) - (dy2 * dz1);
            this.B = (dz1 * dx2) - (dz2 * dx1);
            this.C = (dx1 * dy2) - (dx2 * dy1);
            this.D = (((dy2 * dz1) - (dy1 * dz2)) * this.vertexes.get(0).getX()) +
                    (((dz2 * dx1) - (dz1 * dx2)) * this.vertexes.get(0).getY()) +
                    (((dx2 * dy1) - (dx1 * dy2)) * this.vertexes.get(0).getZ());
            /* texting of finded parametrs  */
            for (Vertex vertex : this.vertexes) {
                double test = this.surfaceValue(vertex);
                if (Math.abs(this.surfaceValue(vertex)) > 1E-2) {
                    this.A = 0;
                    this.B = 0;
                    this.C = 0;
                    this.D = 0;
                    right = false;
                    index ++;
                }
            }
            if(index == this.vertexCount) {
                this.active = false;
                return false;
            }
        }
        return true;
    }

    /**
     * Test of layng projection on face within polygon forming by face's set of vertex
     * @param projection coordinates of projection
     * @return true - if laying, else - false
     */
    private boolean poliginTest(Vertex projection) {
        double summAngle = 0;
        for(int i = 0; i < this.vertexes.size(); i ++){
            Vertex vector1 = new Vertex();
            Vertex vector2 = new Vertex();
            vector1.setX(this.vertexes.get(i).getX() - projection.getX());
            vector1.setY(this.vertexes.get(i).getY() - projection.getY());
            vector1.setZ(this.vertexes.get(i).getZ() - projection.getZ());
            vector2.setX(this.vertexes.get((i + 1) % this.vertexes.size()).getX() - projection.getX());
            vector2.setY(this.vertexes.get((i + 1) % this.vertexes.size()).getY() - projection.getY());
            vector2.setZ(this.vertexes.get((i + 1) % this.vertexes.size()).getZ() - projection.getZ());
            double dist1 = Math.sqrt(Math.pow(vector1.getX(), 2) + Math.pow(vector1.getY(), 2) + Math.pow(vector1.getZ(), 2));
            double dist2 = Math.sqrt(Math.pow(vector2.getX(), 2) + Math.pow(vector2.getY(), 2) + Math.pow(vector2.getZ(), 2));
            double scalar1 = (vector1.getX() * vector2.getX()) + (vector1.getY() * vector2.getY()) + (vector1.getZ() * vector2.getZ());
            summAngle += Math.acos(scalar1 / (dist1 * dist2));
        }
        if(Math.abs(2*Math.PI - summAngle) <= 0.01) {
            return true;
        }
        return false;
    }

    /**
     * Return projection of vertex on face or null
     * @param vertex
     * @return vertex with coordinates of projection on face or null
     */
    public Vertex getVertexProjection(Vertex vertex) {
        Vertex projection = new Vertex();
        //lamda param
        double lamda = -(this.D + this.A * vertex.getX() + this.B * vertex.getY() + this.C * vertex.getZ()) / (Math.pow(this.A, 2) + Math.pow(this.B, 2) + Math.pow(this.C, 2));
        //coords of projection of vertex on this face
        projection.setX(vertex.getX() + this.A * lamda);
        projection.setY(vertex.getY() + this.B * lamda);
        projection.setZ(vertex.getZ() + this.C * lamda);
        /* testiong */
        if(this.poliginTest(projection)) {
            return projection;
        }
        return null;
    }

}
