/**
 * Root of ModelComparer
 */
public class Main {

    public static void main(String args[]) {
        Comparer comparer = new Comparer();
        /* check count of arguments */
        if(args.length < 3) {
            System.out.print("Ошибка! Не указаны один или несколько параметрв");
            System.exit(-1);
        }
        String modelPath = args[0];
        String vertexCloudPath = args[1];
        String reportPath = args[2];
        String outerConfig = "";
        if(args.length > 3) {
            outerConfig = args[3];
        }
        /* load outer config coefs */
        CompareConfig.loadCoefs(outerConfig);
        /* comparing */
        comparer.Compare(modelPath, vertexCloudPath, reportPath);
    }






}
