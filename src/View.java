import com.threed.jpct.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Threading window for 3D-renderiung
 */
public class View implements Runnable {

    /**
     * Max vertex count to visualization
     */
    private static final int maxVisualVertex = 2500;

    /**
     * Run threading window
     */
    public void run() {
        this.show();
    }

    /**
     * Coordinates of view center
     */
    private SimpleVector viewCenter;

    /**
     * Camera spheric coords
     */
    private double cameraRadius = 50; /* distanse from origin */
    private double cameraFiAngle = Math.PI / 2; /* radius-vector rotaton in XOY face */
    private double cameraOmegaAngle = 0; /* radius-vector angle between XOY face and Ox axis */

    /**
     * 3D rendering window and world
     */
    private World world; /* World */
    private FrameBuffer buffer;
    ; /* buffer */
    private JFrame frame; /* Window frame */

    /**
     * Constructor of class
     *
     * @param title
     */
    public View(String title) {
        this.viewCenter = new SimpleVector(0, 0, 0);
        /* prepearing window */
        frame = new JFrame(title);
        frame.setSize(800, 600);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        /* prepearing 3D sciene */
        world = new World();
        world.setAmbientLight(255, 255, 255);
        /* adding event listeners */
        MouseDragListener dr = new MouseDragListener();
        this.frame.addMouseListener(dr);
        this.frame.addMouseMotionListener(dr);
        this.frame.addComponentListener(new ResizeEventListener());
        this.frame.addMouseWheelListener(new MouseWheelListener());
        this.frame.addWindowListener(new CloseEventListener());
    }

    /**
     * Set camera coords
     *
     * @param x
     * @param y
     * @param z
     */
    public void setViewCenter(float x, float y, float z) {
        this.viewCenter.x = x;
        this.viewCenter.y = y;
        this.viewCenter.z = z;
    }

    /**
     * Dispaly voxels array
     *
     * @param voxels
     * @param scale
     */
    public void loadVoxelsArray(ArrayList<Voxel> voxels, float scale) {
        int visualCoef = 1;
        if (voxels.size() > this.maxVisualVertex) {
            visualCoef = (int) (voxels.size() / this.maxVisualVertex) + 1;
        }
        for (int i = 0; i < voxels.size(); i++) {
            if (i % visualCoef == 0) {
                Object3D box = Primitives.getCube(scale * (float) voxels.get(i).getEdge());
                box.setTexture("texture");
                if (voxels.get(i).hasVertexes()) {
                    box.setAdditionalColor(0, 255, 0);
                } else {
                    box.setAdditionalColor(255, 0, 0);
                }
                box.translate(scale * (float) voxels.get(i).getCenter().getX(),
                        scale * (float) voxels.get(i).getCenter().getY(),
                        scale * (float) voxels.get(i).getCenter().getZ());
                box.build();
                world.addObject(box);
            }
        }
    }

    /**
     * Display polygonal model
     *
     * @param faces model faces array
     * @param scale
     */
    public void loadFacesArray(ArrayList<Face> faces, float scale) {
        int visualCoef = 1;
        if (faces.size() > this.maxVisualVertex) {
            visualCoef = (int) (faces.size() / this.maxVisualVertex) + 1;
        }
        for (int i = 0; i < faces.size(); i++) {
            Object3D polygon = new Object3D(faces.get(i).getVertexCount());
            for (int j = 0; j < faces.get(i).getVertexCount(); j++) {
                Vertex vertex1 = faces.get(i).getVertexes().get(j);
                Vertex vertex2 = faces.get(i).getVertexes().get((j + 1) % faces.get(j).getVertexCount());
                Vertex vertex3 = faces.get(i).getVertexes().get((j + 2) % faces.get(j).getVertexCount());
                //vectors
                SimpleVector vector1 = new SimpleVector(scale * vertex1.getX(), scale * vertex1.getY(), scale * vertex1.getZ());
                SimpleVector vector2 = new SimpleVector(scale * vertex2.getX(), scale * vertex2.getY(), scale * vertex2.getZ());
                SimpleVector vector3 = new SimpleVector(scale * vertex3.getX(), scale * vertex3.getY(), scale * vertex3.getZ());
                polygon.addTriangle(vector1, vector2, vector3);
            }
            polygon.setTexture("texture");
            if (faces.get(i).getVoxelsIndexes().size() < 1) {
                polygon.setAdditionalColor(255, 0, 0);
            } else {
                if (faces.get(i).getFilledVoxelsCount() < 1) {
                    polygon.setAdditionalColor(255, 255, 0);
                } else {
                    polygon.setAdditionalColor(0, 255, 0);
                }
            }
            polygon.setEnvmapped(Object3D.ENVMAP_ENABLED);
            polygon.compile();
            polygon.build();
            world.addObject(polygon);
        }
    }

    /**
     * Display vertex cloud
     * @param vertexes
     * @param scale
     */
    public void loadVertexsArray(ArrayList<Vertex> vertexes, float scale) {
        int visualCoef = 1;
        if (vertexes.size() > this.maxVisualVertex) {
            visualCoef = (int) (vertexes.size() / this.maxVisualVertex) + 1;
        }
        for (int i = 0; i < vertexes.size(); i++) {
            if (i % visualCoef == 0) {
                Object3D point = Primitives.getCube(0.2f);
                point.setTexture("texture");
                point.setAdditionalColor(vertexes.get(i).getRed(),
                        vertexes.get(i).getGreen(),
                        vertexes.get(i).getBlue());
                point.translate(scale * (float) vertexes.get(i).getX(),
                        scale * (float) vertexes.get(i).getY(),
                        scale * (float) vertexes.get(i).getZ());
                point.setEnvmapped(Object3D.ENVMAP_ENABLED);
                point.build();
                world.addObject(point);
            }
        }
    }

    /**
     * Displaing process
     * <p>
     * Can finish programm with status -1
     */
    public void show() {
        buffer = new FrameBuffer(frame.getWidth(), frame.getHeight(), FrameBuffer.SAMPLINGMODE_NORMAL);
        try {
            while (this.frame.isShowing()) {
                this.cameraControl();
                buffer.clear(new Color(200, 200, 200));
                world.renderScene(buffer);
                world.draw(buffer);
                buffer.update();
                buffer.display(frame.getGraphics());
            }
        } catch (Exception e) {
            System.exit(-1);
        }
        System.exit(0);
    }

    /**
     * Camera control
     */
    private void cameraControl() {
        SimpleVector position = new SimpleVector();
        position.x = (int) (this.cameraRadius * Math.cos(this.cameraFiAngle) * Math.sin(this.cameraOmegaAngle));
        position.y = (int) (this.cameraRadius * Math.sin(this.cameraFiAngle) * Math.sin(this.cameraOmegaAngle));
        position.z = (int) (this.cameraRadius * Math.cos(this.cameraOmegaAngle));
        world.getCamera().setPosition(position);
        world.getCamera().lookAt(new SimpleVector(viewCenter));
    }

    /**
     * Window resize event listener
     */
    private class ResizeEventListener extends ComponentAdapter {

        @Override
        public void componentResized(ComponentEvent e) {
            buffer = new FrameBuffer(frame.getWidth(), frame.getHeight(), FrameBuffer.SAMPLINGMODE_NORMAL);
        }
    }

    /**
     * Close event listener
     */
    private class CloseEventListener extends WindowAdapter {

        @Override
        public void windowClosed(WindowEvent e) {
            buffer.disableRenderer(IRenderer.RENDERER_OPENGL);
            buffer.dispose();
            frame.dispose();
        }
    }

    /**
     * Mouse Wheel event listener
     */
    private class MouseWheelListener extends MouseAdapter {

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (cameraRadius < 1 && e.getWheelRotation() < 0) {
                return;
            }
            cameraRadius += e.getWheelRotation();
        }
    }

    /**
     * Mouse drag event listener
     */
    private class MouseDragListener extends MouseAdapter {
        /**
         * Mouse left button pressed flag
         */
        boolean leftPressed = false;

        /**
         * Last mouse coords
         */
        Point lastMouseCoords = new Point();

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.leftPressed = true;
                lastMouseCoords = new Point(e.getX(), e.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                this.leftPressed = false;
            }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (this.leftPressed) {
                cameraOmegaAngle += (Math.PI / 180) * (this.lastMouseCoords.getY() + e.getY()) / 300;
                cameraFiAngle += (Math.PI / 180) * (this.lastMouseCoords.getX() + e.getX()) / 300;
            }
        }
    }
}


