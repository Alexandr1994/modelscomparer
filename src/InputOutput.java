import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Input Output helper class
 */
public class InputOutput {

    /**
     * Checking file
     * @param filename path to file
     * @return true file exists, else false
     */
    public static boolean fileTesting(String filename) {
        File file = new File(filename);
        if(file.exists() && file.isFile() && file.canRead()) {
            return true;
        }
        return false;
    }

    /**
     * Return file parser
     * @param format format of file
     * @return Parser object or null, if unsupported format
     */
    private static Parser getParser(String format)
    {
        switch (format) {
            case "ply": { //ply format
                return new PlyParser();
            }
            case "stl": {
                return new StlParser();
            }
            case "txt": {
                return new TxtParser();
            }
            default: {
                return null;
            }
        }
    }

    /**
     * Load vertex cloud
     * @param fileName
     * @return 3D-object  container
     */
    public static Container3D loadVertexCloud(String fileName) {
        return load(fileName, false);
    }

    /**
     * load model or voxel array
     * @param filename
     * @return 3D-object  container
     */
    public static Container3D loadModel(String filename) {
        return load(filename, true);
    }

    /**
     * Constructor of class
     * <p>
     * Load vertex cloud and model (if flag isModel true) from file with name filename
     * @param filename path to file
     * @param isModel flag of loading model
     * @return Container of 3D-object if success, else - null
     */
    private static Container3D load(String filename, boolean isModel) {
        /* check the file */
        if(!fileTesting(filename)) {
            return null;
        }
        Container3D container = new Container3D();
        /* format */
        String format = filename.substring(filename.lastIndexOf(46) + 1);
        format = format.toLowerCase();
        Parser parser = getParser(format);
        if (parser != null && parser.load(filename)) {
            container.uploadVertexCloud(parser.getLoadedVertexCloud());
            container.constructBorderValues();
            if (isModel) {
                if (parser.voxelsLoaded) {
                    container.uploadVoxelsArray(parser.getLoadedVoxelArray());
                } else {
                    //segments counts
                    container.uploadModel(parser.getLoadedModel());
                    //voxels
                }
            }
            return container;
        } else {
            return null;
        }
    }

    /**
     * Save vertex cloud in ply-file in ascii encoding
     * <p>
     * Debug special method
     * <p>
     * Can finish programm with exit code -5
     * @param filename path to file
     * @param vertexCloud array of saving vertexes
     */
    public static void saveCloud(String filename, ArrayList<Vertex> vertexCloud) {
        try {
            /* header */
            RandomAccessFile file = new RandomAccessFile(filename, "rw");
            file.writeChars("ply\n");
            file.writeChars("format ascii 1.0\n");
            file.writeChars("element vertex " + vertexCloud.size() + "\n");
            file.writeChars("property float x\n");
            file.writeChars("property float y\n");
            file.writeChars("property float z\n");
            file.writeChars("property float nx\n");
            file.writeChars("property float ny\n");
            file.writeChars("property float nz\n");
            file.writeChars("element face 0\n");
            file.writeChars("property list uchar uint vertex_indices\n");
            file.writeChars("end_header\n");
            String pattern = "##0.000000";
            DecimalFormat format = new DecimalFormat(pattern);
            /* saving vertexes */
            for(Vertex vertex : vertexCloud) {
                file.writeChars(format.format(vertex.getX()) + " " + format.format(vertex.getY()) + " " + format.format(vertex.getZ()) + " 0.000000 0.000000 0.000000\n");
            }
            file.close();
        }
        catch (Exception e) {
            System.out.print("Ошибка записи файла!");
            System.exit(-5);
        }
    }

    /**
     * Load outer configuration file
     * @param filename path to file
     * @return Map of correcton coefficients
     */
    public static HashMap<String, Double> loadOuterConfig(String filename) {
        HashMap<String, Double> outerConfig = new HashMap<String, Double>();
        try {
            RandomAccessFile file = new RandomAccessFile(filename, "r");
            String confStr = file.readLine();
            while (confStr != null) {
                String[] confOpt = confStr.split(" ");
                outerConfig.put(confOpt[0], Double.parseDouble(confOpt[1]));
                confStr = file.readLine();
            }
            file.close();
            return outerConfig;
        } catch (Exception e) {
            return outerConfig;
        }
    }

    /**
     * Save report file
     * <p>
     * Can finish programm with exit code -4
     * @param filename path to file
     * @param reportString report content
     * @return true - success, else - false
     */
    public static boolean savingReport(String filename, String reportString) {
        try{
            FileWriter writer = new FileWriter(filename);
            writer.write(reportString);
            writer.close();
        }
        catch(Exception e) {
            System.out.print("Ошибка создания файла-отчета!");
            System.exit(-4);
        }
        return true;
    }


}
