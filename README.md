##What is ModelsComparer?
ModelsComparer is application for finding percent of compliance of vertex cloud as result 3D scanning of some real object to sample 3D-model.
Application can work with noisy input data and inconsistent positions, sizes and orientation of vertex cloud and sample 3D-model.
In addition, outer correction of configuration coefficients is provided for correction of some errors in analysis, scaling, composition and rotation.
Analyze is completing on support axis Oz of 3D space with help of constructing array of voxels which will repeat form of sample 3D-model

##Supported formats of input files
Sample 3D-model and vertex cloud can be:

* polygonal file format or ply-files in ascii or binary(in any byte order) internal encoding
* stereolitographic file format or stl-files in ascii or binary internal encoding
* sequence of vertex coordinates in txt-file (only for vertex clouds)

##What is outer configuration?
Outer configuration it just text file with extension “.cnf” containing definition for next parameters:

Variable in cnf-file | Range of acceptable values | Default value | Function
---------------------|-----------------------------|---------------|-------------------------
thin_coef | (0…5) | 1 | Noises vertex trim correction coefficient
scale_coef | (0…3) | 1 | Scaling correction coefficient
precision | (0…1) | 0,1 | Analyze precision (size of a voxel)

Outer configuration purpose is supresion of analysis error that could be  caused by noises and mismatches of positions, sizes and orientations of analyzed 3D-model and vertex cloud.

##How it works?
You can start ModelsComparer using terminal or another application.
To start ModelsComparer you should provide values for the following arguments:

*	path to file of sample 3D-model
*	path to file of vertex cloud
*	path to results folder
*	optional, path to file of outer configuration

```

java –jar <path to sample 3D model> <path to vertex cloud> <path to reports dir> [<path to outer configuration>]

```

As a result of program execution a report file with data about analysis (Russian only yet)  will be formed and windows with visualization of results of analysis will be shown.

![Visualization of results of analize](img/screen.png)